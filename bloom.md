# Bloom's Taxonomy

[Bloom's Taxonomy][bloom] is used by educators in crafting
[learning outcomes][outcomes]. Some hints for its use follow,
based on work by Nelson Baker at Georgia Tech:
<nelson.baker@pe.gatech.edu>.

## Remember

Keywords
: list, recite, outline, define, name, match, quote, recall,
  identify, label, recognize
  
Example
: _By the end of this lesson, the student will be able to_
  recite Newton’s three laws of motion.

## Understand

Keywords
: describe, explain, paraphrase, restate, give original examples of,
  summarize, contrast, interpret, discuss
  
Example
: _By the end of this lesson, the student will be able to_
  describe Newton’s three laws of motion to in her/his own words
  
## Apply

Keywords
: calculate, predict, apply, solve, illustrate, use, demonstrate,
  determine, model, perform, present
  
Example
: _By the end of this lesson, the student will be able to_
  calculate the kinetic energy of a projectile.

## Analyze

Keywords
: classify, break down, categorize, analyze, diagram, illustrate,
  criticize, simplify, associate
  
Example
: _By the end of this lesson, the student will be able to_
  differentiate between potential and kinetic energy.
  
## Evaluate

Keywords
: choose, support, relate, determine, defend, judge, grade, compare,
  contrast, argue, justify, support, convince, select, evaluate
  
Example
: _By the end of this lesson, the student will be able to_
  determine whether using conservation of energy or conservation of
  momentum would be more appropriate for solving a dynamics problem.
  
## Create

Keywords
: design, formulate, build, invent, create, compose, generate,
  derive, modify, develop
  
Example
: _By the end of this lesson, the student will be able to_
  design an original homework problem dealing with the principle
  of conservation of energy.

<!-- links -->

[bloom]: https://en.wikipedia.org/wiki/Bloom's_taxonomy
[outcomes]: https://tips.uark.edu/using-blooms-taxonomy/
