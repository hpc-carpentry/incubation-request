## Acceptance message from The Carpentries

From Toby Hodges, cc'd to Nisha Ghatak, SherAaron Hurt, Kari Jordan.

E-mails and calendly links suppressed for privacy.

Hi HPC Carpentry Steering Committee members,

Thanks for your patience as we went through the process of updating The Carpentries policy on Lesson Program Incubation. I am pleased to share the updated checklist for lesson projects in [Incubation](https://docs.carpentries.org/topic_folders/governance/lesson-program-policy.html#phase-2-incubation>), published in our community handbook at the end of last week following approval from the Program Committee of our Board of Directors (Nisha Ghatak, chair of that committee, is in cc).  

To summarise the changes:

- we have grouped requirements for lesson projects into categories, each annotated with a brief description of the overall goal of the requirements described there 
- some of the existing requirements have been expanded on, to give more detail about what The Carpentries needs to receive from the lesson project
- a few new requirements have been added, mostly with the aim of demonstrating an active and growing community around the curriculum of the project
- a new set of optional materials has been listed, which could support the application for Lesson Program adoption

Please read through the list on the page linked above, and send me your questions, or we can discuss at an upcoming community meeting, or you can schedule a call with me.

Finally, the policy now states that "the Lesson Project must accomplish specific requirements within a defined time frame (maximum 18 months) to be agreed upon between the Lesson Project and The Carpentries." We can set this time frame beginning June 2024, in recognition of the time you have been waiting for us to come back to you with more information. Would you like to set a deadline of 10 December 2025 i.e. the maximum 18 months? Or would you prefer to set a closer target date?

Thanks,

Toby

