<!--
author:
- Andrew Reid ([@reid-a]), [NIST], USA
- Trevor Keller ([@tkphd]), [NIST], USA
- Annajiat Alim Rasel ([@annajiat]), [BracU], Bangladesh
- Benson Muite ([@bkmgit]), [Kichakato Kizito], Kenya
- Alan O'Cais ([@ocaisa]), [CECAM], Spain
- Wirawan Purwanto ([@wirawan0]), [ODU], USA
- Rohit Goswami ([@haozeke]), [Quansight Labs] & [U. Iceland]
date: April 28, 2023
-->

# Request for Clarification

On 24 April 2023, we received the following response from The Carpentries.

> Greetings,
>
> On behalf of the Program Committee of the Carpentries' Executive Council,
> thank you for applying for lesson program incubation. We have reviewed your
> application and feel you have met the core criteria. To bring this topic to a
> vote at the quarter two Executive Council meeting (May 11th), we ask that you
> provide clarification by responding to the questions below via email by May
> 1st.
>
> 1. Can you clarify the domain knowledge needed to teach HPC Carpentry workshops?
>    We recommend writing it in [Bloom's taxonomy][bloom] format.
> 2. You could pull from the learning objectives from the lessons.
> 3. There are two audiences for HPC workshops: users and developers.
>    Do you envision eventual lessons for HPC managers?
> 4. Please provide clarity about how the lessons align with domain knowledge.
>    Should it be domain agnostic or not? For example, Software Carpentry is
>    domain agnostic, but Data Carpentry has a domain associated with each
>    curriculum.
> 5. Please clarify: What lessons would be core, and what is extra for this
>    Lesson Program? Can you give us some example workshop schedules?
> 6. Can you give us an estimate of how many people have taken the pilot workshops?
> 7. Can you write a paragraph or link to issues where you used the feedback
>    from learners AND instructors to improve the lessons/workshops?
> 8. Please provide more detail about how the existing structure will integrate
>    into the Carpentries, especially with roles from the steering committee
>    and the roles in Lesson Program Governance Committee.
>
> Thank you so much for all your work.
> Cheers,
> Yanina Bellini Saibene
> Program Committee Chair

Our response follows.

---

### Domain Knowledge

[HPC Carpentry] workshops revolve around dispatching parallel computational
workloads on shared resources, i.e. clusters of computers. The software
frameworks used to dispatch these jobs are known as workload managers, or
"queuing systems" more colloquially. The queuing system has its own abstraction
of the cluster of computers and the programs being executed on it. This state
of this abstraction is represented in terms like "nodes" (meaning computers),
"jobs" (meaning running programs), "accounts" (meaning users), "resources"
(meaning the number of CPUs, amount of available memory, or allowable runtime)
on each compute node, and "queues" or "partitions" (meaning subsets of
computers with similarly configured hardware or other resources).

While the lesson materials contain most of the specifics, we consider the
following domain knowledge essential to a successful workshop. We are open to,
and intend to undertake, documentation of a lot of this knowledge in Instructor
Onboarding materials, but have not developed those resources yet.

1. ___Remember:___ the Instructor should be able to...

   * name the queuing system and the cluster's gateway or login computer.
   * list the available filesystems and recognize whether they are shared
     across the network or not.
   * recite the commands and common flags required to launch a job through the
     queuing system, e.g., `sbatch` for Slurm or `qsub` for PBS/Torque.
   * outline the terms assigned to relevant elements of state for the queuing
     system.

2. ___Understand:___ queuing system commands can have different effects. Jobs
   are submitted to the queue, but a job can be deferred or fail due to
   interactions with competing jobs and system resources. The "same data
   everywhere" character of shared file systems also means that file system
   commands can have effects on machines where they were not issued. The
   Instructor should be able to...

   * paraphrase this asynchronous character and explain the difference between
     running a job directly (like opening a program on their laptop) and how
     the queuing system "launches" a job on the compute node.
   * discuss where a program will run -- on the local machine, on the login
     node, or on a compute cluster -- and whether it is likely to launch
     immediately, after some time, or not at all.

3. ___Apply:___ the Instructor should be able to use the queuing system to...

   * interrogate the number of compute nodes and their status (available to
     accept new work or not).
   * determine the number of jobs in the queue.
   * assess whether new jobs will "launch" immediately.
   * predict whether the cluster can accommodate a given resource request,
     e.g., a job requesting 100 TB of memory on a single node can never
     launch.
   * determine the number of CPU cores (or other resource) a particular job is
     using.

4. ___Analyze:___ the state of the queuing system has different scopes, so that
   different modes of interaction are available to the user who launched a job
   _vs._ everyone else. The Instructor should be able to...

   * analyze whether a computational task is expected to consume more
     resources than it requested.
   * break down the impact on performance and other user's work when such a
     misbehaved job is running.
   * illustrate how to find a job that has impact outside its bounds, and what
     to do about it (cancel if it's your job, contact an admin if it is not).

5. ___Evaluate:___ the queuing system is an additional layer of abstraction
   between the user and the computer program they wish to run. Our lesson
   material contains some metaphors and explanations attempting to link its
   operation and intent to concepts more familiar to Learners' everyday
   lives. The Instructor should be able to...

   * relate the function of the queuing system to familiar concepts, such as
     the provided restaurant example wherein the queuing system takes the role
     of _maître d'_.
   * map the operation of the queuing system to goals outside the system.

6. ___Create:___ the end goal of the workshop is to create a shell script that
   captures an intended workflow and executes it on the cluster, rather than on
   the Learner's local machine or on the cluster's login node. The Instructor
   should be able to...

   * build strategies for mapping real-world goals ("I want to learn X
     about my data") to operational system goals ("I want the system to run a job
     that performs analysis Y on my data, to learn X.").

### Curriculum Audiences

At present, we intend to craft HPC Carpentry workshops for two audiences: users
(focused on the queuing system and how to manage jobs), and programmers (how to
write, profile, and optimize software to effectively use cluster resources).

A third potential audience is, as mentioned, HPC managers or "system
administrators," responsible for building, provisioning, maintaining, and
upgrading computer clusters to address institutional demands. At present, and
for the foreseeable future, the nuances of building a cluster to fill a
particular niche make it difficult to justify the effort required to establish
an effective curriculum.

### HPC Lessons and Domain Alignment

We have designed the HPC Carpentry curriculum to exercise the queuing system
and teach Learners how to launch and manage jobs in a reproducible fashion. The
nature of the jobs being launched is immaterial: in fact, the current lessons
use a "dummy" code that sleeps for a specified fraction of runtime to simulate
work being done, without actually doing work (or using additional power).

With that being said, the planned HPC Programming track needs a specific
problem to implement, and developing materials with a specific domain in mind
will help to reduce conceptual overload. We intend to leverage the modular
nature of The Carpentries' lesson infrastructure to support a few domains of
relatively general interest, following Data Carpentry's lead.

Additional modules may be domain specific since they may focus on software used
in a particular domain. HPC has many community codes, for example for weather,
computational chemistry, engineering, etc. Some of these may form the basis for
lessons. The Carpentries' lesson infrastructure is especially helpful for
community codes that arise out of research projects and are used in research.

### Core vs. Auxiliary Lessons

[HPC Intro] is the heart of HPC Carpentry: it introduces fundamental cluster
computing concepts and teaches the Learner how to interact with a cluster
through the queuing system. HPC Intro builds on Software Carpentry's Shell
Novice lesson, since the command line interface is fundamental to all cluster
resources. (Some implement web-based front-ends as well, but the command line
is universal.) [HPC Workflows] builds on HPC Intro in turn, teaching how to
convert a hand-crafted script into a reproducible workflow that can
automatically regenerate missing outputs. However, some audiences will be less
interested in workflow management, and more engaged in using container
environments, machine learning, GPU programming, etc. We therefore propose to
offer workshops built on HPC Intro and a number of lessons that are externally
maintained (at the moment), working with the maintainers to put an HPC spin on
things as needed.

The canonical Workshop for HPC Novices comprises:

1. [Shell Novice] from Software Carpentry (Morning, Day 1)
2. [HPC Intro] (Afternoon, Day 1 and Morning, Day 2)
3. [HPC Workflows] (Afternoon, Day 2)

An alternative workshop for novice HPC users could focus on containerized
workflows:

1. HPC Intro (Day 1)
2. [Singularity Intro] from the Incubator (Day 2)

A workshop for intermediate HPC users could focus on LAMMPS,
an established and widely used molecular dynamics framework:

1. Shell Novice (Morning, Day 1)
2. HPC Intro (Afternoon, Day 1 and Morning, Day 2)
3. [Tuning LAMMPS] (Afternoon, Day 2)

A workshop for novice HPC programmers could build on the learners'
interests and teach:

1. HPC Intro (Day 1)
2. [HPC Parallel Novice] or [HPC Chapel] (Day 2)

### Number of Participants

HPC Carpentry workshops have been piloted at the following institutions:

* National Institute of Standards and Technology (Washington, DC):
  * October 2022:
    10 Learners, 2 Instructors (both HPC Carpenters)
  * April 2022:
    10 Learners, 2 Instructors (both HPC Carpenters)
* University of Mauritius (Réduit, Moka)
  * August 2021:
    20 Learners, 2 Instructors (both HPC Carpenters), and 1 Helper (external)
* Brac University (Dhaka)
  * October 2021:
    20 Learners, 2 Instructors (both HPC Carpenters), and 3 Helpers (external)
  * February 2021:
    20 Learners and 2 Instructors (both HPC Carpenters)

### Incorporating Feedback

[hpc-intro/390] outlines feedback from a workshop held at NIST.  That feedback
partially motivated (and fully prioritized) [hpc-intro/411], which refactored
the entire lesson to use our [Amdahl] program as-is, instead of live-coding a
parallel $\pi$ calculator in Python. This refactoring followed extensive
discussion of the merits of live-coding Python _vs._ job scripts, and helped
our community to clarify the scope and outcomes of the lessons.

We also have less sweeping examples, such as [hpc-intro/297], representing a
discussion of grammar in our lessons and linking to pull requests that were
merged as a result. The comment thread represents good community engagement in
some gritty details of lesson development.

### Integration Into The Carpentries

We are committed to embracing The Carpentries' [Lesson Program Policy]. For the
past two years, our [Steering Committee] has operated as a Round Table of
"Members," with Andrew Reid and Trevor Keller fulfilling the functions assigned
to the Chair and Co-Chair roles in the Policy (calling meetings and setting
agendas), with notetaking on CodiMD carried out by attendees.

If HPC Carpentry is inducted into the Lesson Program Incubator, we intend to
rename our Steering Committee to the Lesson Program Governance Committee and
create a Curriculum Advisory Committee, each with the required roles (Chair,
Co-Chair, and Members). As annual elections for the existing Steering Committee
are due in May, the first order of business would be to hold democratic
elections via GitHub to fill the seats on both Committees, both of which shall
have five seats.

<!-- links -->

[bloom]: https://en.wikipedia.org/wiki/Bloom's_taxonomy
[Amdahl]: https://github.com/hpc-carpentry/amdahl
[HPC Carpentry]: https://www.hpc-carpentry.org
[HPC Chapel]: https://github.com/hpc-carpentry/hpc-chapel
[HPC Intro]: https://github.com/carpentries-incubator/hpc-intro
[HPC Parallel Novice]: https://github.com/hpc-carpentry/hpc-parallel-novice
[HPC Workflows]: https://github.com/carpentries-incubator/hpc-workflows
[Lesson Program Policy]: https://docs.carpentries.org/topic_folders/governance/lesson-program-policy.html
[Shell Novice]: https://github.com/swcarpentry/shell-novice
[Singularity Intro]: https://github.com/carpentries-incubator/singularity-introduction
[Steering Committee]: https://www.hpc-carpentry.org/contact/
[Tuning LAMMPS]: https://github.com/hpc-carpentry/tuning_lammps

[hpc-intro/297]: https://github.com/carpentries-incubator/hpc-intro/issues/297
[hpc-intro/390]: https://github.com/carpentries-incubator/hpc-intro/issues/390
[hpc-intro/411]: https://github.com/carpentries-incubator/hpc-intro/pull/411
