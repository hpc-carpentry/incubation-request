## HPC Carpentries Reply

From the HPC Carpentry steering commitee, to Toby Hodges, cc Nisha Ghatak, SherAaron Hurt, Kari Jordan.

Hi Toby and Carpentries Curriculum Development team,

Thanks so much for your message of June 10, and for the associated informal conversations, and apologies for the delay in getting you a formal reply.

We are delighted to engage with your new process, we understand the need for new lesson programs to demonstrate technical aptitude, service to the community, and value to the greater Carpentries effort.

We look forward to meeting these requirements by 10 December 2025. 

Thanks,

The HPC Carpentry steering committee
