<!--
author:
- Andrew Reid ([@reid-a]), [NIST], USA
- Trevor Keller ([@tkphd]), [NIST], USA
- Annajiat Alim Rasel ([@annajiat]), [BracU], Bangladesh
- Benson Muite ([@bkmgit]), [Kichakato Kizito], Kenya
- Alan O'Cais ([@ocaisa]), [CECAM], Spain
- Wirawan Purwanto ([@wirawan0]), [ODU], USA
- Susan Branchett ([@sebranchett]), [TU Delft], The Netherlands
- Rohit Goswami ([@haozeke]), [Quansight Labs] & [U. Iceland]
date: March 16, 2023
-->

# HPC Carpentry and The Carpentries

"High-performance computing" or HPC refers to clusters of computers,
interconnected by low-latency networks, used to perform parallel
computing tasks that are intractable on personal computer resources.
Examples include large-scale fluid dynamics simulations ("virtual
windtunnels") that require many terabytes of memory capacity and the
concerted efforts of thousands of processors to complete in a
reasonable amount of time, training of large machine learning models
that require the concerted effort of thousands of general-purpose
graphics processing units (GPUs) to perform the required linear
algebra, and countless varieties of simultaneous program execution to
sweep a parameter space, enumerate crystal or protein structures, or
integrate systems of differential equations that defy analytical
solution.

HPC resources are scientific instruments requiring institutional
investment to obtain and training to effectively use.
As off-the-shelf hardware has improved in quality and fallen in price
over the decades, the leverage such machinery can apply to scientific
discovery has both increased and become more accessible to researchers
at institutions great and small.
Simultaneously, the operating systems and interfaces of personal
computing machinery have introduced an ever-deepening stack of
abstractions between the user and the hardware.
These two trends conspire to make the learning curve for new HPC users
ever steeper:
you must use computational resources to perform almost any research
task, yet the modes of interaction native to most HPC clusters are
entirely foreign to anyone raised in the era of smartphones and web
portals.

The core concept behind HPC Carpentry is to close this gap.

## Who We Are

The HPC Carpentry project is a group of volunteers who are working on
developing a number of lessons, closely aligned with the goals of The
Carpentries generally, for the specific use-case of novice users of
high-performance computing resources.
These resources offer the benefit of access to large sets of compute
resources, and benefits arising from the ability to run computational
tasks in parallel, but also require a different skill set than that
associated with the kind of serial computations typically done on
laptops or local workstations.

Our developer community consists of a number of contributors with
domain expertise in the HPC world, and a number who are in educational
roles at universities, with some overlap.
This community has persisted across at least one major leadership
change (the departure of Peter Steinbach from a central role in the
effort), and has a wide range of informal connections to HPC providers
and site operators, such as Compute Canada.

## Our Goals

The principal goal of the effort is to make both beginner-friendly and
accurate technical material available to new users of HPC resources.
This will improve training outcomes for users, ease the user
on-boarding burden for site operators, and help democratize HPC usage
in the face of more widespread and cheaper computational resources.
Our lessons are public, free, and open-source, and we aspire to the
kind of continuous improvement that arises from learner and instructor
feedback that is characteristic of The Carpentries.

## Governance and Lesson Improvement

The HPC Carpentry group has a steering committee which is formally
elected from the larger group by a GitHub-mediated process.
The steering committee nominally controls who has write access to the
lessons and can approve pull requests, but in practice contributors
have mostly been drawn from a small group, and approval has been
collegial and relatively informal.
The steering committee also sets strategic goals, primarily by
consensus, and coordinates community activities like the recent SC21
BoF drafts, and the composition of this document.

Our lesson improvement model is essentially that of The Carpentries
generally.
Our lessons are in GitHub repositories, and we collect positive and
negative feedback from workshop organizers, instructors, and learners
after workshops, and make an effort to translate this feedback into
actionable issues on the repositories.


## Existing Content and Strategic Plan

Currently, we have two major lessons at a reasonably mature stage of
development.
They are [HPC Intro], which introduces learners to basic operations
on shared HPC resources, from remote log-in to parallel job
submission;
and [HPC Workflows], which uses the Snakemake tool to demonstrate
workflow automation on HPC systems while reinforcing core concepts
from The Carpentries' Python and Shell lessons.
HPC Intro has been taught many times by members of HPC Carpentry
around the world, both as add-ons to Carpentries-organized workshops
and at stand-alone HPC Carpentry events.

Our strategic plan has these lessons as part of two types of workshop:
one centered around users, and another centered around prospective HPC
code developers.
The user workshop will make use of the existing Unix Shell lesson,
followed by the HPC Intro lesson, and then follow up with the
workflow lesson.
The developer workshop will start the same way, with the Unix Shell
and HPC Intro, but will follow with a yet-to-be-developed lesson
about how to write codes that run well on HPC systems.

In addition to the lessons intended for the workshops, we are aware
of a variety of other content that cover topics extremely relevant to
the HPC space and which could be used to complement an HPC Carpentry
workshop.
Examples of active such lessons include:

- [Introduction to Singularity][lesson-singularity]
- [Introduction to Deep Learning][lesson-deep-learning]
- [Parallel Programming in Python][lesson-parallel-python]

In addition, we have spoken to the maintainers of several external
lessons, and an HPC Carpentry workshop may draw from or teach the
following lessons:

- [MPI Tutorial][mpitutorial]
- [ODU: DeapSecure][deapsecure]
- [R4HPC: Programming with Big Data in R][R4HPC]

## State of the HPC Carpentry Community

The HPC Carpentry Steering Committee is elected annually following a
GitHub-based asynchronous democratic process.
The current members are:

- Alan O'Cais  ([@ocaisa]), [CECAM], Spain
- Andrew Reid ([@reid-a]), [NIST], USA
- Annajiat Alim Rasel ([@annajiat]), [BracU], Bangladesh
- Rohit Goswami ([@haozeke]), [Quansight Labs] & [U. Iceland]
- Trevor Keller ([@tkphd]), [NIST], USA
- Wirawan Purwanto ([@wirawan0]), [ODU], USA

Members of the Steering Committee organize public meetings twice
monthly to coordinate and execute on shared goals, write abstracts
and proposals for community outreach (including sessions at
CarpentryCon), and contribute to curriculum and lesson development
efforts through the GitHub-based workflows typical of Carpentries
projects.

Coordination and communication between community members occurs on
[GitHub][hpcc-gh] (31 members), [Slack][hpcc-sl] (215 members),
[TopicBox][hpcc-tb], [Twitter][hpcc-tw] (56 followers), and through
the HPC Carpentry [website][hpcc-web].

Over the past year, HPC Carpentry [workshops][hpcc-workshops] have
been taught in Dublin, Ireland; Miami, Florida, USA; Dhaka,
Bangladesh; Berlin, Germany; Moka, Mauritius; and Gaithersburg,
Maryland, USA.


### A brief history of HPC Carpentry

Compute Canada (now the Digital Research Alliance of Canada) created
the original HPC Carpentry organization on GitHub and handed over
control of that organization to the HPC Carpentry steering committee
in 2021.

The current versions of the HPC Carpentry lessons have two original
sources:

- The original set of HPC Carpentry lessons created by Compute
  Canada, especially the
  [Introduction to High-Performance Computing][original-intro-hpc]
- Peter Steinbach's [HPC-in-a-Day][hpc-in-a-day] lesson

It was agreed by the Steering Committee to make a concerted effort to
merge these two sources which has resulted in the current iteration
of the [Introduction to HPC lesson][HPC Intro].

In general, there has been consistent and extensive interest among
both the supercomputing community and the Carpentries community in
the (potential) activities of HPC Carpentry.
This can be seen by the participation by HPC Carpentry in events
related to both communities:

- [BoF at SC17][bof-sc17] (Andy Turner, Christina Koch, Tracy Teal,
  Bob Freeman, and Chris Bording)
- [BoF at CarpentryCon 2018][bof-cc18] (Alan O'Cais and Daniel Smith)
- [Meetup at SC18][bof-sc18] (Andy Turner, Christina Koch, Peter
  Steinbach, Alan O'Cais, Jeffrey Stafford, John Simpson, Daniel
  Smith, Bob Freeman, and Trevor Keller)
- [BoF at CarpentryCon 2020@Home][bof-cc20] (Alan O'Cais and Peter
  Steinbach)
- [BoF at SC21][bof-sc21] (Andrew Reid and the HPC Carpentry Steering
  Committee)
- Breakout, [Lightning Talk][talk-cc22], and Sprint at CarpentryCon
  2022 (HPC Carpentry Steering Committee and Benson Muite)

Over its lifetime, HPC Carpentry has also navigated a number of
"changing of the guards", with key contributors such as Peter
Steinbach, Andy Turner and Christina Koch departing, and new
contributors arriving (as evidenced by the make-up of the current
Steering Committee).

### The Broader Community

Over its lifetime, there have been a number of forks of the content,
taken at various stages in its development.
A recent outreach effort to the owners of these forks has yielded
some interest.
A small community of interest surrounding the content exists.
Engaging with this community may provide valuable feedback, and may
be a resource we can draw on for future curriculum advice.

## HPC Workshop Requirements

### Resources

During an HPC Carpentry lesson, it will be necessary for learners to
access an actual HPC cluster, whether it is a permanent installation
or an ephemeral instance for instructional purposes.
Lesson organizers may need to coordinate with an HPC site operator,
or may need to stand up an instructional cluster on cloud resources.
We are aware of tools which accomplish this latter task, but access
to the necessary cloud resources may be an issue.

HPC Carpentry can guarantee access to a cloud-based resource until
the end of 2023.
This resource will be used as the default for HPC Carpentry
workshops, particularly in the case of online/hybrid workshops.

### Instructor Knowledge

Lesson organizers will also need to ensure that learners can log in,
possibly including creating accounts for learners.
Instructors should be familiar with the layout and peculiarities of
the particular cluster they are using, so that their live-typing
procedure will actually work, and to deal with anomalies learners may
encounter.
Instructors do not need to be HPC experts themselves, but ideally
should be experienced users of clusters in general, and have a basic
familiarity with the instructional cluster.

Currently, instructors are drawn from the HPC Carpentry developer
community, including the owners of various forks.
As the program expands, we will want to draw instructors from the
larger Carpentries community, and will need to ensure that they have
access to the specialized knowledge required.
We expect to provide on-boarding material, and will also encourage
learners to become helpers and instructors.

### Lesson Customization

In addition, because of the variable nature of HPC resources, and the
requirement that instructors will live-type commands that are
appropriate to the resource, our lessons must be configurable so that
references to the cluster head node or other site-specific resources
are correct.
To this end, our lessons incorporate a "snippet library" which can be
customized on a per-site basis.
This is a deviation from the usual Carpentries lesson structure.

## Summary

Becoming an official Carpentries lesson will enrich The Carpentries
portfolio by adding HPC content in an organized, accessible way, and
will enlarge and broaden the audience for our material, as well as
increasing the amount (and quality) of feedback, leading to greater
improvement of the content.

<!-- authors -->
[@annajiat]: https://github.com/annajiat
[@bkmgit]:   https://github.com/bkmgit
[@haozeke]:  https://github.com/haozeke
[@ocaisa]:   https://github.com/ocaisa
[@reid-a]:   https://github.com/reid-a
[@sebranchett]: https://github.com/sebranchett
[@tkphd]:    https://github.com/tkphd
[@wirawan0]: https://github.com/wirawan0

<!-- affiliations -->
[BracU]: https://www.bracu.ac.bd
[CECAM]: https://www.cecam.org
[Kichakato Kizito]: http://kichakatokizito.solutions
[NIST]: https://www.nist.gov
[ODU]: https://www.odu.edu
[Quansight labs]: https://labs.quansight.org
[U. Iceland]: https://english.hi.is

<!-- lessons -->
[HPC Intro]: https://github.com/carpentries-incubator/hpc-intro
[HPC Workflows]: https://github.com/carpentries-incubator/hpc-workflows

[hpcc-gh]: https://github.com/hpc-carpentry/
[hpcc-sl]: https://swcarpentry.slack.com/archives/CEXAZR52T
[hpcc-tb]: https://carpentries.topicbox.com/groups/discuss-hpc
[hpcc-tw]: https://twitter.com/hpccarpentry
[hpcc-web]: https://www.hpc-carpentry.org
[hpcc-workshops]: http://www.hpc-carpentry.org/past-workshops/
[hpc-in-a-day]: https://github.com/psteinb/hpc-in-a-day

[original-intro-hpc]: https://github.com/hpc-carpentry/hpc-intro/releases/tag/v9.1.2
[lesson-deep-learning]: https://github.com/carpentries-incubator/deep-learning-intro
[lesson-parallel-python]: https://github.com/carpentries-incubator/lesson-parallel-python
[lesson-singularity]: https://carpentries-incubator.github.io/singularity-introduction/

[deapsecure]: https://deapsecure.gitlab.io/lessons/
[mpitutorial]: https://mpitutorial.com
[R4HPC]: https://github.com/RBigData/R4HPC

<!-- meetings -->
[bof-sc17]: https://sc17.supercomputing.org/SC17%20Archive/bof/bof_pages/bof125.html
[bof-cc18]: https://github.com/carpentries/carpentrycon/blob/main/CarpentryCon-2018/Sessions/2018-05-31/05-Breakout-8-HPC-Carpentry/Abstract.md
[bof-sc18]: https://aturner-epcc.github.io/presentations/SC18_BoF_Carpentry_Nov2018/
[bof-cc20]: https://www.youtube.com/watch?v=Xo3Ab2GFj24
[bof-sc21]: https://sc21.supercomputing.org/presentation/?id=bof161&sess=sess403
[talk-cc22]: https://www.youtube.com/watch?v=k6ybjZb471w&list=PLXLapl_LKb4cjs0740iWwxmZpN18EgPRl&index=11
